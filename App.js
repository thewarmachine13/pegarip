import React, { useEffect, useState } from 'react';
import { Text, View, FlatList, ScrollView, NetInfo } from 'react-native';

export default () => {
  const [result, setResult] = useState(null);
  const [allGetIds, setIds] = useState([]);

  useEffect(async () => {
    let { setItem, getItem } = require('react-native').AsyncStorage;
    let IDdata = [];
    let ids = await getItem('IDdata');

    if (ids)
      IDdata = JSON.parse(ids);

    NetInfo.isConnected.fetch().then(isConnected => {
      if (isConnected) {
        fetch('https://ipapi.co/json/').then((response) => {
          response.json().then(value => {
            value.date = new Date();

            setResult(value);
            let esteveaqui = IDdata.some(item => item.ip == value.ip);

            if (esteveaqui)
              setIds(IDdata);
            else {
              let salvar = [...IDdata, value];
              setItem('IDdata', JSON.stringify(salvar))
              setIds(salvar);
            }
          }).catch(err => {
            setIds(IDdata)
          })
        })
      } else
        setIds(IDdata)
    });
  }, []);

  if (!result) return <View style={{ backgroundColor: 'red' }}></View>

  if (allGetIds) {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView>

          <View style={[styles.container, { paddingBottom: 50, backgroundColor: 'lightgray' }]}>
            {result &&
              <>
                <Text>Dados atuais</Text>
                <Text>IP:{result.ip}</Text>
                <Text>ORG:{result.org}</Text>
                <Text>ASN: {result.asn}</Text>
                <Text>Linguas: {result.languages}</Text>
                <Text>Moeda: {result.currency}</Text>
                <Text>Timezone: {result.timezone}</Text>
                <Text>Codigo de area: {result.country_calling_code}</Text>
                <Text>utc_offset: {result.utc_offset}</Text>
                <Text>Cidade: {result.city}</Text>
                <Text>Pais: {result.country_name}</Text>
                <Text>Pais Abreviado: {result.country}</Text>
                <Text>Latitude: {result.latitude}</Text>
                <Text>Longitude: {result.longitude}</Text>
                <Text>Região: {result.region}</Text>
                <Text>Codigo Região: {result.region_code}</Text>

              </>
            }
          </View>

          <FlatList
            data={allGetIds}
            extraData={allGetIds}
            keyExtractor={(item, index) => `${index}`}
            renderItem={({ item }) => <View style={[styles.container, { paddingVertical: 20, backgroundColor: 'gray' }]}>
              <Text>IP:{item.ip}</Text>
              <Text>ORG:{item.org}</Text>
              <Text>ASN: {item.asn}</Text>
              <Text>Linguas: {item.languages}</Text>
              <Text>Moeda: {item.currency}</Text>
              <Text>Timezone: {item.timezone}</Text>
              <Text>Codigo de area: {item.country_calling_code}</Text>
              <Text>utc_offset: {item.utc_offset}</Text>
              <Text>Cidade: {item.city}</Text>
              <Text>Pais: {item.country_name}</Text>
              <Text>Pais Abreviado: {item.country}</Text>
              <Text>Latitude: {item.latitude}</Text>
              <Text>Longitude: {item.longitude}</Text>
              <Text>Região: {item.region}</Text>
              <Text>Codigo Região: {item.region_code}</Text>

            </View>
            }
          />
        </ScrollView>
      </View >
    )
  }
}
const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  }
};
